#!/bin/bash

. macupdate.conf

NEWLOC=`curl -I "https://www.macupdate.com/download/${MACUPDATE_APP_ID}" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15' 2>/dev/null | grep location | sed 's/location: //' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
